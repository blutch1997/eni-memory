let userNameValid = false;
let emailValid = false;
let passwordValid = false;
let confirmPasswordValid = false;


window.onload = init();

function init(){

    document.getElementById("userName").addEventListener("input", verrifierUserName);
    document.getElementById("eMail").addEventListener("input", verifierMail);
    document.getElementById("mdp").addEventListener("input", verifierMdp);
    document.getElementById("mdpConfirm").addEventListener("input", verifierMdpConfirm);

    validForm();
}

function verrifierUserName(event){
    let userNameOk = event.currentTarget.value.length;
    if(userNameOk >= 3){
        document.getElementById("userNameImage").src = "images/check.svg";
        userNameValid = true;
    } else {
        document.getElementById("userNameImage").src = "images/error.svg";
    }

    validForm();
}

function verifierMail(event){
    let saisie = event.currentTarget.value;

    let mailOk = saisie.match(/^\S+@\S+\.\S+$/g);

    if(mailOk){
        document.getElementById("eMailImage").src = "images/check.svg";
        emailValid = true;
    } else {
        document.getElementById("eMailImage").src = "images/error.svg";
    }
    
    validForm();
}

function verifierMdp(event){
    let saisie = event.currentTarget.value; //le contenu du champ input

    //controle des différentes contraintes
    let lettresOk = verifierLettres(saisie);
    let specialCaracOk = verifierSpecialCarac(saisie);
    let chiffreOk = verifierChiffre(saisie);
    let tailleOk = verifierTaille(saisie);

    //Application des règles d'affichage en fonction des différentes contraintes respectées
    if(lettresOk && specialCaracOk && chiffreOk && tailleOk){
        document.getElementById("mdpImage").src = "images/check.svg";
        passwordValid = true;
    } else {
        document.getElementById("mdpImage").src = "images/error.svg";
    }

    validForm();
}

function verifierMdpConfirm(event){
    let saisie = event.currentTarget.value; //le contenu du champ input

    //controle des différentes contraintes
    let lettresOk = verifierLettres(saisie);
    let specialCaracOk = verifierSpecialCarac(saisie);
    let chiffreOk = verifierChiffre(saisie);
    let tailleOk = verifierTaille(saisie);

    //Vérification de si la confirmation du mot de passe est le même que le mot de passe
    if(lettresOk && specialCaracOk && chiffreOk && tailleOk && document.getElementById("mdp").value === document.getElementById("mdpConfirm").value){
        document.getElementById("confirmMdpImage").src = "images/check.svg";
        confirmPasswordValid = true;
    } else {
        document.getElementById("confirmMdpImage").src = "images/error.svg";
    }

    if(saisie.length < 6){
        let mdpStrengthDiv = document.getElementById("mdpStrength");
        
    }

    validForm();
}


function verifierLettres(saisie){
    return saisie.match(/[A-Za-z]/g) != null;
}

function verifierSpecialCarac(saisie){
    return saisie.match(/[$-/:-?{-~!"^_`\[\]]/g) != null;
}

function verifierChiffre(saisie){
    return saisie.match(/[0-9]/g) != null;
}

function verifierTaille(saisie){
    return saisie.length >= 6;
}

function validForm(){
    if(userNameValid && emailValid && passwordValid && confirmPasswordValid){
        document.getElementById("validerButton").disabled = false;
    } else {
        document.getElementById("validerButton").disabled = true;
    }
}


function toAccueil(){
    window.location.href = "accueil.html";
}
function toConnexion(){
    window.location.href = "connexion.html";
}
function valider(){
    window.location.href = "connexion.html";
}