

let imagesTable = [];
let srcImage1 = null;
let targetImage1;
let srcImage2 = null;
let nbPaires = 0;
let nbCoups = 0;

window.onload = init();

function init(){
    
    imagesTable = affichageTableau();
    shuffleTable(imagesTable);
    affichageInitial();
    actualisationNbCoups(nbCoups);   

    let img0 = document.getElementById("0");
    let img1 = document.getElementById("1");
    let img2 = document.getElementById("2");
    let img3 = document.getElementById("3");
    let img4 = document.getElementById("4");
    let img5 = document.getElementById("5");
    let img6 = document.getElementById("6");
    let img7 = document.getElementById("7");
    let img8 = document.getElementById("8");
    let img9 = document.getElementById("9");
    let img10 = document.getElementById("10");
    let img11 = document.getElementById("11");    

    img0.addEventListener("click", comparaison);
    img1.addEventListener("click", comparaison);
    img2.addEventListener("click", comparaison);
    img3.addEventListener("click", comparaison);
    img4.addEventListener("click", comparaison);
    img5.addEventListener("click", comparaison);
    img6.addEventListener("click", comparaison);
    img7.addEventListener("click", comparaison);
    img8.addEventListener("click", comparaison);
    img9.addEventListener("click", comparaison);
    img10.addEventListener("click", comparaison);
    img11.addEventListener("click", comparaison);

    window.addEventListener("keydown", actualiser)
}



// Création d'un tableau conservant les images tirées au lancement
function affichageTableau(){    
    let currentImage;
    let currentImage2;
    for(i = 0; i < 6; i++){
        let random = Math.trunc(Math.random() * 6) + 1;
        currentImage = document.getElementById(i);
        currentImage2 = document.getElementById(i + 6);
        currentImage.src = "images/memory-legume/" + random + ".svg";
        currentImage2.src = "images/memory-legume/" + random + ".svg";
        imagesTable.push(currentImage.src);
        imagesTable.push(currentImage2.src);
    }
    return imagesTable;
}

// Mélange des images dans le tableau
function shuffleTable(imagesTable){
    let currentIndex = imagesTable.length,  randomIndex;

    // While there remain elements to shuffle.
    while (currentIndex != 0) {

        // Pick a remaining element.
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [imagesTable[currentIndex], imagesTable[randomIndex]] = [
        imagesTable[randomIndex], imagesTable[currentIndex]];
    }

    return imagesTable;
}


// Remplacement de toutes les images par le point d'interrogation
function affichageInitial(){
    for(i=0; i<12; i++){
        document.getElementById(i).src = "images/question.svg";
    }
}

function comparaison(event){
    event.currentTarget.style.transform = "rotateY(180deg)";
    event.currentTarget.style.transition = "all 0.3s ease";

    let id = event.target.id;
    if(!srcImage2){
        event.target.src = imagesTable[id];
        if(srcImage1){
            srcImage2 = event.target.src;
        } else {
            targetImage1 = event.target;
            srcImage1 = event.target.src;
        }
    }    
    
    
    if(srcImage2){
        if(srcImage1 !== srcImage2){
            setTimeout(retournerImage, 2000, event.target);
            setTimeout(retournerImage, 2000, targetImage1);
            document.getElementById("memoryDiv").style.pointerEvents = "none";
        } else {
            nbPaires++;
            if(nbPaires == 6){
                document.getElementById("memoryDiv").style.pointerEvents = "none";
                let victoryDiv = document.getElementById("victoryDiv")
                let victoryParaph = document.createElement("p");
                let victoryText = document.createTextNode("VICTORY!")

                victoryParaph.setAttribute("id", "victoryParaph");

                victoryDiv.appendChild(victoryParaph);
                victoryParaph.appendChild(victoryText);
            }
        }

        srcImage1 = null;
        srcImage2 = null;
        nbCoups++;
        actualisationNbCoups(nbCoups);
    }
}

function retournerImage(target){

    target.style.transform = "rotateY(0deg)";
    target.style.transition = "all 0.3s ease";
    target.src = "images/question.svg";
    document.getElementById("memoryDiv").style.pointerEvents = "all";
   
}

function actualiser(event){
    if(event.key == " "){
        location.reload();
    }   
}

function actualisationNbCoups(nbCoups){
    document.getElementById("nbCoups").innerHTML = "Nombre de coups: " + nbCoups;
    console.log("coucou");
}

function toAccueil(){
    window.location.href = "accueil.html";
}

function toInscription(){
    window.location.href = "inscription.html";
}